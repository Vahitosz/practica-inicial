from django.db import models

from django.contrib.auth.models import User


class ClaseModelo(models.Model):
    estado = models.BooleanField(default=True)  
    #  fecha crea
    fc = models.DateTimeField(auto_now_add=True)
    # fecha modifica
    fm = models.DateTimeField(auto_now=True)
    # usuario crea
    uc = models.ForeignKey(User, on_delete=models.CASCADE)
    # usuaro modifica
    um = models.IntegerField(blank=True,null=True)

    class Meta:
        abstract = True