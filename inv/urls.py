from django.urls import path

from .views import CategoriaView, CategoriaNew,CategoriaEdit, \
    CategoriaDel, \
        SubCategoriaView, SubCategoriaNew, SubCategoriaEdit, SubCategoriaDel, \
            MarcaView, MarcaNew, MarcaEdit, MarcaDel, marca_inactiva, \
                UMView, UMNew, UMEdit, UMDel, um_inactiva, \
                    ProductoView, ProductoNew, ProductoEdit, producto_inactivar
                

urlpatterns = [
    path('categorias/', CategoriaView.as_view(), name='categoria_list'),
    path('categorias/new/', CategoriaNew.as_view(), name='categoria_new'),
    path('categorias/edit/<int:pk>/', CategoriaEdit.as_view(), name='categoria_edit'),
    path('categorias/delete/<int:pk>/', CategoriaDel.as_view(), name='categoria_del'),

    path('subcategoria/', SubCategoriaView.as_view(), name='subcategoria_list'),
    path('subcategoria/new/', SubCategoriaNew.as_view(), name='subcategoria_new'),
    path('subcategoria/edit/<int:pk>/', SubCategoriaEdit.as_view(), name='subcategoria_edit'),
    path('subcategorias/delete/<int:pk>/', SubCategoriaDel.as_view(), name='subcategoria_del'),

    path('marca/', MarcaView.as_view(), name='marca_list'),
    path('marca/new/', MarcaNew.as_view(), name='marca_new'),
    path('marca/edit/<int:pk>/', MarcaEdit.as_view(), name='marca_edit'),
    path('marca/delete/<int:pk>/', MarcaDel.as_view(), name='marca_del'),
    path('marca/inactiva/<int:id>/', marca_inactiva, name='marca_inactiva'),

    path('um/', UMView.as_view(), name='um_list'),
    path('um/new/', UMNew.as_view(), name='um_new'),
    path('um/edit/<int:pk>/', UMEdit.as_view(), name='um_edit'),
    path('um/delete/<int:pk>/', UMDel.as_view(), name='um_del'),
    path('um/inactiva/<int:id>/', um_inactiva, name='um_inactiva'),

    path('productos/', ProductoView.as_view(), name='producto_list'),
    path('productos/new/', ProductoNew.as_view(), name='producto_new'),
    path('productos/edit/<int:pk>/', ProductoEdit.as_view(), name='producto_edit'),
    path('productos/inactiva/<int:id>/', producto_inactivar, name='producto_inactiva'),
]



