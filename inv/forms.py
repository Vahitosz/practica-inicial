from django import forms

from .models import Categoria, SubCategoria, Marca, UnidadMedida, Producto


class CategoriaForms(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ['descripcion', 'estado']
        labels = {'descripcion':"Descripcion de la Categoria",
                "estado":"Estado"}
        widget = {'descripcion': forms.TextInput}

    def __int__(self, *args, **kwargs):
        super().__int__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class SubCategoriaForms(forms.ModelForm):
    categoria = forms.ModelChoiceField(
        queryset = Categoria.objects.filter(estado=True)
        .order_by('descripcion')
    )
    class Meta:
        model = SubCategoria
        fields = ['categoria', 'descripcion', 'estado']
        labels = {'descripcion':"SubCategoria",
                "estado":"Estado"}
        widget = {'descripcion': forms.TextInput}

    def __int__(self, *args, **kwargs):
        super().__int__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        self.fields['categoria'].empty_label = "Seleccione la categoria"
        
class MarcaForms(forms.ModelForm):
    class Meta:
        model = Marca
        fields = ['descripcion', 'estado']
        labels = {'descripcion':"Descripcion de la marca",
                "estado":"Estado"}
        widget = {'descripcion': forms.TextInput}

    def __int__(self, *args, **kwargs):
        super().__int__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class UMForms(forms.ModelForm):
    class Meta:
        model = UnidadMedida
        fields = ['descripcion', 'estado']
        labels = {'descripcion':"Descripcion de medida",
                "estado":"Estado"}
        widget = {'descripcion': forms.TextInput}

    def __int__(self, *args, **kwargs):
        super().__int__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class ProductoForms(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ['codigo', 'codigo_barra', 'descripcion', 'estado', \
                'precio', 'existencias', 'ultima_compra', 
                'marca',  'subcategoria', 'unidad_medida']
    
        exclude = ['um', 'fm', 'uc', 'fc']
        widget = {'descripcion': forms.TextInput()}

    def __int__(self, *args, **kwargs):
        super().__int__(*args,**kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        self.fields['ultima_compra'].widget.attrs['readonly'] = True
        self.fields['existencia'].widget.attrs['readonly'] = True